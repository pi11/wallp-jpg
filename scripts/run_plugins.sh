#!/bin/bash
# run plugins

dir=$(dirname $(which $0));
cd $dir
cd ..
source ../ve/bin/activate

mypidfile=/tmp/wallp-jpg-run-plugins.pid
PID=$(<"$mypidfile")

if kill -0 $PID > /dev/null 2>&1;
then
   echo "Script is already runing, pid:[$PID]" >&2
   exit 1
fi

echo $$ > "$mypidfile"
python manage.py run_plugins

# Ensure PID file is removed on program exit.
trap "rm -f -- '$mypidfile'" EXIT

