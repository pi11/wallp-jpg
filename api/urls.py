# -*- coding: utf-8 -*-
from django.conf.urls import url
from api.views import random_images

urlpatterns = [
    url(r'^random/$',
        random_images, name="random_images"),
    ]
