# -*- coding: utf-8 -*-
import json
from random import randint

from django.db.models import Max
from django.http import HttpResponse
from django.shortcuts import render

from images.models import ImageCache, Image


def random_images(request):
    try:
        c = int(request.GET.get('c', 3))
    except ValueError:
        c = 3
    if c > 6:
        c = 5

    max_h = ImageCache.objects.all().aggregate(Max('h_id'))['h_id__max']
    ids = [str(randint(0, max_h)) for i in range(0, c)]

    image_ids = [ic.i_id.pk for ic in ImageCache.objects.filter(h_id__in=ids)]
    
    images = Image.objects.filter(is_active=True, pk__in=image_ids)
    res = []
    
    for im in images:
        if im.gif:
            url = im.image.url
            print("GIF")
        else:
            url = im.get_thumb_big()
        res.append({"image":url, "url":im.get_absolute_url(), "pk":im.pk})
    res = json.dumps(res)
    return HttpResponse(res, content_type='application/json')

