from django.urls import include, path
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    path(r'', include('images.urls')),
    path(r'api/', include('api.urls')),
    path(r'admin/', admin.site.urls),
    path('i18n/', include('django.conf.urls.i18n')),    
]
