#-*- coding: utf-8 -*-

from datetime import timedelta, datetime, date
import re
import traceback
from pyquery import PyQuery as pq
from images.functions import process_image
from djangohelpers.url import load_url
from urlparse import urljoin
import string
from images.models import Tag


def do_parse_page(url, is_top=False):
    def rm_junk(t):
        res = ""
        sd = "%s%s " % (string.letters, string.digits)
        for a in t:
            if a in sd:
                res = "%s%s" % (res, a)
        return res

    data = load_url(url)
    new_content = False
    if data:
        d = pq(data)
        blocks = d.items('a.replylink')
        new = False
        for b in blocks:
            url = urljoin('https://boards.4chan.org/wg/', b.attr('href'))

            print "Ext post:%s" % url
            post = load_url(url)
            dd = pq(post)

            tags_bl = rm_junk(dd('blockquote.postMessage:first').text())

            tags_list = list(set(tags_bl.lower().strip().split(' ')))
            print 'Tags:', tags_list

            img_blocks = dd('a.fileThumb').items()
            for im in img_blocks:
                img_src = im.attr('href')
                if img_src is not None and ".webm" not in img_src:
                    img_url = "https:%s" % img_src
                    im = process_image(img_url, tags_list)

    else:
        print "Can't load %s" % url
    return new_content


def handle(p=1, only_top=False):
    """Load images"""
    return do_parse_page('https://boards.4chan.org/wg/')
