#-*- coding: utf-8 -*-

from datetime import timedelta, datetime, date
import re
import traceback
from pyquery import PyQuery as pq
from images.functions import process_image
from djangohelpers.url import load_url
from urlparse import urljoin
import string
from images.models import Tag
import json

def do_parse_page(url, is_top=False):
    def rm_junk(t):
        res = ""
        sd = "%s%s " % (string.letters, string.digits)
        for a in t:
            if a in sd:
                res = "%s%s" % (res, a)
        return res

    data = load_url(url)
    
    new_content = False
    if data:
        d = json.loads(data)
        for b in d['data']:
            print b
            url = "http://nik.bot.nu/st.fu?req=mode:json%20action:view%20id:{0}%20safe:null".format(b['id'])
            print "Ext post:%s" % url
            post = load_url(url)
            
            dd = json.loads(post)
            
            img_src = "http://nik.bot.nu/o%s" % dd['path']
            print "img_src: %s" % img_src
            tags = rm_junk(dd['name'].lower().strip()).split(' ')
            for t in dd['tags']:
                tags.append(t['name'].strip().lower())
            tags = list(set(tags))
            
            print 'Tags:', tags
            
            im = process_image(img_src, tags)
                    
    else:
        print "Can't load %s" % url
    return new_content

def handle(p=1, only_top=False):
    """Load images"""
    return do_parse_page('http://nik.bot.nu/wt.fu?req=mode:json%20sort:random')

