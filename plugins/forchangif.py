#-*- coding: utf-8 -*-

from datetime import timedelta, datetime, date
import re
import traceback
from pyquery import PyQuery as pq
from images.functions import process_image
from djangohelpers.url import load_url
    
def do_parse_page(url, is_top=False):
    data = load_url(url)
    new_content = False
    if data:
        d = pq(data)
        urls = d.items('a.replylink')
        for url in urls:
            l = "https://boards.4chan.org/gif/%s" % url.attr('href')
            print "Loading %s" % l
            data = load_url(l)
            dd = pq(data)
            images = dd.items('a.fileThumb')
            for im in images:
                print im.attr('href')
                img_src = "http:%s" % im.attr('href')
                if img_src.endswith('webm'):
                    print "Skiping webm"
                    continue

                new = process_image(img_src)
                if new:
                    new_content = True
    else:
        print "Can't load %s" % url
        print b, c
    return new_content

def handle(p=1, only_top=False):
    """Load videos"""
    return do_parse_page('https://boards.4chan.org/gif/%s' % p)

