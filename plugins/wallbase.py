#-*- coding: utf-8 -*-

from datetime import timedelta, datetime, date
import re
import traceback
from pyquery import PyQuery as pq
from images.functions import process_image
from djangohelpers.url import load_url
from urlparse import urljoin
import string
from images.models import Tag


def do_parse_page(url, is_top=False):

    data = load_url(url)
    new_content = False
    if data:
        d = pq(data)
        blocks = d.items('figure')
        new = False
        for b in blocks:
            url = b('a:last').attr('href')

            print "Ext post:%s" % url

            post = load_url(url)
            dd = pq(post)
            img_src = dd('img#wallpaper').attr('src')

            tags_bl = dd('ul#tags').items('li a')
            tags = []
            for t in tags_bl:
                tags.append(t.text().lower().strip())
            tags = list(set(tags))
            print 'Tags:', tags

            im = process_image(img_src, tags)

    else:
        print "Can't load %s" % url
    return new_content


def handle(p=1, only_top=False):
    """Load images"""
    return do_parse_page('http://alpha.wallhaven.cc/random?page=1')
