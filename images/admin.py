# -*- coding: utf-8 -*-
# Admin configuration for images module
 
from django.contrib import admin
from images.models import *

class SourceAdmin(admin.ModelAdmin):
    """
    Class for Source Admin  interface
    """    
    list_display = ('url', 'is_active', 'last_checked', 'interval', 'error')


class ImageAdmin(admin.ModelAdmin):
    search_fields = ("imageinfo__tags__name",)
    list_display = ("__unicode__", "get_admin_thumb", "is_active",
                    "added", "gif", "nsfw", "xrate")
    list_filter = ("is_active", "gif", "nsfw")


class SimiliarImageAdmin(admin.ModelAdmin):
    list_display = ("__unicode__", "thumb1", "thumb2")


class ImageTitleAdmin(admin.ModelAdmin):
    pass

class BlockedTagAdmin(admin.ModelAdmin):
    pass

class TagAdmin(admin.ModelAdmin):
    list_display = ("name", "ru_name", "count")

class PluginAdmin(admin.ModelAdmin):
    list_display = ("name", "last_start", "last_update", 
                    "status", "runing", "frequency", "error")
    
admin.site.register(Source, SourceAdmin)
admin.site.register(SimiliarImage, SimiliarImageAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(ImageTitle, ImageTitleAdmin)
admin.site.register(Plugin, PluginAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(BlockedTag, BlockedTagAdmin)

