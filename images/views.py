# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, date
import time
from random import randint

from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.models import Q, Max
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import Http404, HttpResponse, HttpResponseRedirect

import json as simplejson
from django.db.models import Sum, Count
from django.views.decorators.cache import never_cache
from django.contrib.auth.models import User
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.shortcuts import redirect

from emailuser.models import EmailUser, EmailUserForm, manual_create_email_user
import imagehash
from djangohelpers.utils import get_page_range, gen_url, wilson_score
from djangohelpers import edge

from images.models import *


def index(request):
    is_edge = edge.is_mobile(request)
    total = Image.objects.count()

    images = Image.objects.filter(is_active=True).order_by("-id")[:30]
    tags = Tag.objects.filter(count__gt=10).order_by('?')[:10]
    ptags = Tag.objects.filter().order_by('-count')[:30]
    # print images.query
    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    page = paginator.page(1)
    colors = Pallete.objects.filter().order_by("?")[:10]
    added_today = Image.objects.filter(
        added__gt=datetime.now() - timedelta(days=1)).count()
    return render(
        request, "index.html", {"is_edge": is_edge, "page": page, "images": page,
                                'tags': tags, 'colors': colors, "total": total,
                                "added_today": added_today,
                                "ptags": ptags})


def sizes(request):

    # sizes = Image.objects.filter(is_active=True).distinct('width',
    # 'height').order_by('-width')
    sizes = []
    return render(request, "sizes.html", {"sizes": sizes})


def colors(request):
    # colors = Pallete.objects.all().order_by("color")
    colors = []
    return render(request, "colors.html", {"colors": colors})


def new(request):

    images = Image.objects.filter(is_active=True).order_by("-id")[:10000]
    # print images.query
    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        raise Http404

    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    page_range = get_page_range(paginator, page_id)
    # print time.time() - start_time, "seconds"
    return render(
        request, "new.html", {"images": page, "page_range": page_range,
                 "page": page})


def popular(request):

    images = Image.objects.filter(
        is_active=True).order_by("-imageinfo__views")[:10000]
    # print images.query
    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        raise Http404

    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    page_range = get_page_range(paginator, page_id)
    # print time.time() - start_time, "seconds"
    return render(
        request, "new.html", {"images": page, "page_range": page_range,
                 "page": page})


def by_size(request, width, height):

    images = Image.objects.filter(is_active=True, width=width,
                                  height=height).order_by("-id")
    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        raise Http404
    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    page_range = get_page_range(paginator, page_id)
    return render(request, "size.html", {"width": width, "height": height,
                                         "images": page, "page": page,
                                         "page_range": page_range})


def color_related(request, image_id):
    raise Http404
    image = get_object_or_404(Image, pk=image_id)
    colors = image.pallete.all()
    palletes = []
    images = Image.objects.filter(is_active=True)
    for c in colors:
        images.filter(Q(pallete__in=c.near_colors.all()) | Q(pallete=c))

    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        raise Http404
    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    page_range = get_page_range(paginator, page_id)

    return render(request, "color-related.html", {"images": page,
                                                  "page": page, "image": image,
                                                  "page_range": page_range})


def by_color(request, color_id):
    raise Http404
    try:
        color = Pallete.objects.get(color='#%s' % color_id)
    except Pallete.DoesNotExist:
        try:
            color = Pallete.objects.get(pk=color_id)
        except (Pallete.DoesNotExist, ValueError):
            raise Http404

    images = Image.objects.filter(is_active=True).filter(
        Q(pallete__in=color.near_colors.all()) | Q(pallete=color)).order_by("-id")
    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        raise Http404
    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    page_range = get_page_range(paginator, page_id)
    return render(request, "color.html", {"color": color, "images": page,
                                          "page": page,
                                          "page_range": page_range})


def by_tag(request, tag_id):
    try:
        tag = Tag.objects.get(pk=tag_id)
    except Tag.DoesNotExist:
        raise Http404

    imageinfo_list = ImageInfo.objects.filter(tags=tag).order_by("-id")
    images = Image.objects.filter(pk__in=[i.id for i in imageinfo_list]).order_by("-id")
    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        raise Http404
    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    page_range = get_page_range(paginator, page_id)
    return render(request, "tag.html", {"tag": tag, "images": page,
                                        "page": page,
                                        "page_range": page_range})


@never_cache
def view_image(request, image_id):
    """View image page"""
    image = get_object_or_404(Image, pk=image_id)
    user = request.user
    try:
        ii = ImageInfo.objects.get(image=image)
    except ImageInfo.DoesNotExist:
        image.delete()
        raise Http404
    if request.META.get('HTTP_REFERER'):
        ii.views = ii.views + 1
        ii.save(update_fields=['views'])
    favs = FavoriteImage.objects.filter(im=image).count()
    if user.is_authenticated():
        is_fav = FavoriteImage.objects.filter(im=image, user=user).count()
    else:
        is_fav = False
    image_titles = ImageTitle.objects.filter(image=image)
    return render(request, "image.html", {"im": image,
                                          "views": ii.views,
                                          "favs": favs,
                                          "is_fav": is_fav
                                          })


def newtitle(request):
    # if request.is_ajax():
    result = {}
    f = ImageTitleForm(request.POST)
    if f.is_valid():
        f.save()
        result['title'] = f.instance.title
    else:
        result['title'] = 'Error'
        print(f.errors)

    json = simplejson.dumps(result)
    return HttpResponse(json, content_type='application/json')


def top(request):
    filters = {}
    filters['is_active'] = True
    rsort = ["-id", ]
    start_from = request.GET.get("start", "")
    try:
        start_from = int(start_from)
    except ValueError:
        start_from = Image.objects.filter(**filters).order_by(*rsort)[0].pk

    sort = "-pvote"
    ref = "top"
    rsort = ["-xrate", "-imageinfo__up_vote",
             "-pvote", "-imageinfo__views", "-id"]
    images = Image.objects.filter(
        is_active=True, imageinfo__up_vote__gt=6).order_by(*rsort)
    # print images.query
    paginator = Paginator(images, 10)
    page_id = int(request.GET.get("p", 1))
    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    if page_id > 1:
        return render(
            request, "image-list.html", {"images": page, "sort": sort, "ref": ref,
                     "top": True, "start_from": start_from})
    else:
        return render(
            request, "index.html", {"images": page, "sort": sort, "ref": ref,
                     "top": True, "start_from": start_from})


def random(request):
    max_h = ImageCache.objects.all().aggregate(Max('h_id'))['h_id__max']
    ids = [str(randint(0, max_h)) for i in range(0, settings.IMAGES_PER_PAGE)]

    images = Image.objects.filter(is_active=True, pk__in=ids)[:settings.IMAGES_PER_PAGE]
    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    page_id = int(request.GET.get("p", 1))
    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False

    images = page
    return render(request, "random.html", locals())


def top_interval(request, interval="today"):
    ref = "top"
    if interval == "today":
        delta = timedelta(days=1)
        ref = "top_today"
    elif interval == "week":
        ref = "top_week"
        delta = timedelta(days=7)
    elif interval == "month":
        ref = "top_month"
        delta = timedelta(days=30)
    else:
        raise Http404
    # votes = Vote.objects.values('image').filter(day_added__gt=date.today()-delta).annotate(Sum('vote'),
    #                                                                                       Count('vote')).order_by("-id")[:100]
    # print votes
    # ids = [im['image'] for im in votes]
    images = Image.objects.filter(
        is_active=True, added__gte=date.today() - delta).order_by("-xrate")[:50]

    filters = {}
    filters['is_active'] = True
    rsort = ["-id", ]
    start_from = request.GET.get("start", "")
    try:
        start_from = int(start_from)
    except ValueError:
        start_from = Image.objects.filter(**filters).order_by(*rsort)[0].pk
    return render(request, "top.html", {"images": images, "top": True,
                                        "start_from": start_from, "ref": ref, })


@never_cache
def vote(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    results = {'success': False}
    v = request.GET.get("v", False)
    if v not in ("0", "2"):
        raise Http404
    v = int(v)
    ip = request.META['REMOTE_ADDR']
    delta = timedelta(days=1)
    nd = date.today() - delta
    try:
        if request.user.is_staff:
            raise Vote.DoesNotExist()
        else:
            vote = Vote.objects.get(ip=ip, image=image,
                                    day_added=date.today())

    except Vote.DoesNotExist:
        if request.user.is_staff:
            pass
        else:
            vote = Vote(image=image, day_added=date.today(), ip=ip,
                        vote=v)
            vote.save()
        ii = image.get_info()
        if v == 0:
            ii.down_vote = ii.down_vote + 1
        elif v == 2:
            ii.up_vote = ii.up_vote + 1
        ii.save()
        image.xrate = wilson_score(ii.up_vote, ii.up_vote + ii.down_vote)
        image.save(update_fields=['xrate', ])
        results['success'] = 1
        results['vote'] = image.get_vote()
        results['total'] = image.get_total_votes()
    else:
        results['success'] = 0

    json = simplejson.dumps(results)
    return HttpResponse(json, content_type='application/json')


def update(request):
    try:
        start_from = int(request.GET.get('pk', False))
    except ValueError:
        start_from = False
    results = {'success': False}
    if start_from:
        last = Image.objects.filter(is_active=True).order_by("-id")[0]
        if last.pk > start_from:
            results['new'] = 1
            new = Image.objects.filter(
                is_active=True, pk__gt=start_from).count()
            results['new_images'] = new
            results['start_from'] = last.pk

    json = simplejson.dumps(results)
    return HttpResponse(json, content_type='application/json')


def login_view(request):
    sended = False
    if request.method == 'POST':
        form = EmailUserForm(request.POST)
        if form.is_valid():  # All validation rules pass
            email = form.cleaned_data['email']
            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                user = User.objects.create_user(
                    email=form.cleaned_data['email'],
                    username=form.cleaned_data[
                        'email'],
                    password=gen_url(20))
                user.save()
            try:
                token = user.emailuser.generate_token()
            except:
                manual_create_email_user(user)
                token = user.emailuser.generate_token()
            # try:
            if settings.DEBUG:
                print("Token - %s" % token)

            send_mail(u'Вход на сайт %s' % settings.SITE_URL,
                      (u'\nДля входа на сайт пройдите по ссылке:'
                       u'%sauth/%s/%s/ \n'
                       u'Если у Вас возникли проблемы со входом '
                       u'напишите письмо на %s' % (settings.SITE_URL,
                                                   user.pk, token,
                                                   settings.SERVER_EMAIL)),
                      u'%s' % settings.SERVER_EMAIL, [email],
                      fail_silently=False)
            sended = True
    else:
        form = EmailUserForm()
    return render(request, "user/login.html", {'form': form, 'sended': sended})


def auth(request, user_id, token):
    get_user = get_object_or_404(User, pk=user_id)
    user = authenticate(email=get_user.email, token=token)
    if user is not None:
        if user.is_active:
            login(request, user)
    return render(request, "user/auth.html", {"user": user})


def logout_view(request):
    logout(request)
    return redirect('index')


def favorite_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    user = request.user
    if user.is_authenticated():
        pass
    else:
        raise Http404

    results = {'success': 1}
    try:
        fi = FavoriteImage.objects.get(im=image, user=user)
    except FavoriteImage.DoesNotExist:
        fi = FavoriteImage(im=image, user=user)
        fi.save()
        results = {'success': 0}
    else:
        fi.delete()
    favs = FavoriteImage.objects.filter(im=image).count()
    results['total'] = favs
    json = simplejson.dumps(results)
    return HttpResponse(json, content_type='application/json')


@login_required
def favorite(request):
    user = request.user
    images = FavoriteImage.objects.filter(user=user).order_by("-added")

    paginator = Paginator(images, settings.IMAGES_PER_PAGE)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        page_id = 1

    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False
    user = request.user
    page_range = get_page_range(paginator, page_id)

    return render(request, "favorite.html", {"images": page,
                                             "page_range": page_range,
                                             "page": page, })


def hash_images(request, hsh, hsh_type):
    # print hsh, hsh_type
    if hsh_type == 'a':
        images = Image.objects.filter(imageinfo__ahash=hsh)
    if hsh_type == 'p':
        images = Image.objects.filter(imageinfo__phash=hsh)
    if hsh_type == 'd':
        images = Image.objects.filter(imageinfo__dhash=hsh)

    paginator = Paginator(images, 100)
    try:
        page_id = int(request.GET.get("p", 1))
    except ValueError:
        page_id = 1

    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False
    user = request.user
    page_range = get_page_range(paginator, page_id)

    return render(
        request, "index.html", {'images': page, "page_range": page_range,
                 "page": page, "user": user})


def find_kind(request, image_id):
    image = get_object_or_404(Image, pk=image_id)

    result = []
    for im in ImageInfo.objects.filter().order_by("-id"):  # check for last 500 images
        if imagehash.hex_to_hash(im.dhash) - imagehash.hex_to_hash(str(image.imageinfo.dhash)) < 3:
            result.append(im.image)
        elif imagehash.hex_to_hash(im.phash) - imagehash.hex_to_hash(str(image.imageinfo.phash)) < 3:
            result.append(im.image)
        elif im.ahash == image.imageinfo.ahash:
            result.append(im.image)

    return render(request, 'find-kind.html', locals())


def get_last_images(request):
    response_data = {}
    response_data['items'] = []
    j = 0
    filters = {}
    filters['is_active'] = True
    filters['pvote__gt'] = 70
    filters['imageinfo__up_vote__gt'] = 2

    images = Image.objects.filter(**filters).order_by("-id")[:3]
    for im in images:
        item = {"page": "%s%s" % (settings.SITE_URL,
                                  reverse('view_image',
                                          kwargs={'image_id': im.pk})
                                  ),
                "image": "%s%s" % (settings.MEDIA_URL, im.image),
                "title": im.title
                }
        j += 1
        response_data['items'].append(item)
    callback = request.GET.get('callback', "")
    return HttpResponse("%s(%s)" % (callback, simplejson.dumps(response_data)), content_type="application/x-javascript")


def redirect_size(request, width, height):
    return HttpResponseRedirect(reverse('by_size', kwargs={"width": width, 'height': height}))


def search_tag(request):
    tag_name = request.GET.get('tag_name', False)
    if not tag_name:
        return HttpResponseRedirect(reverse('index',))
    tags = Tag.objects.filter(
        name__startswith=tag_name.lower()).order_by("-count")
    return render(request, 'search.html', {"tags": tags, "tag_name": tag_name})

def live(request):
    return render(request, 'live.html', {})
