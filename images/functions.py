#-*- coding: utf-8 -*-

from urlparse import urlparse
import hashlib
import PIL
import imagehash
from datetime import date
import os

from django.db.models import Q
from django.conf import settings
from colorweave import palette

from images.models import *
from djangohelpers.url import load_url
from djangohelpers.utils import gen_url


def process_image(ex_url, tags=[]):
    """Process image by URL"""

    def save(data, filename):
        """Save file"""
        try:
            f = open(filename, 'w+')
            f.write(data)
            f.close()
        except IOError, e:
            print("Error saving file %s" % filename)
            pass

    def get_date_path():
        """Generate path by date"""
        d = date.today()
        return "%s-%s-%s" % (d.year, d.month, d.day)

    # check blocked tags
    for bt in BlockedTag.objects.all():
        for t in tags:
            if bt.name.lower() in t.lower():
                print ("Blocked tag, skip image")
                return False
            
    try:
        ii = ImageInfo.objects.get(external_url=ex_url)
    except ImageInfo.DoesNotExist:
        data = load_url(ex_url)
        if not data:
            print("Can't load %s" % ex_url)
            # print b, c
            return False
    else:
        return False
    new = False

    path = "%s%s" % (settings.MEDIA_ROOT, get_date_path())
    try:
        os.makedirs(path)
    except (OSError, IOError):
        pass
    path = urlparse(ex_url).path
    ext = os.path.splitext(path)[1]

    filename = "%s/%s%s" % (get_date_path(), gen_url(10), ext)
    filepath = "%s%s" % (settings.MEDIA_ROOT, filename)
    print("Saving to %s" % filepath)
    save(data, filepath)

    try:
        im = PIL.Image.open(filepath)
    except IOError:
        return False

    ahash = imagehash.average_hash(im)
    dhash = imagehash.dhash(im)
    phash = imagehash.phash(im)

    try:
        ii = ImageInfo.objects.get(dhash=dhash, phash=phash)
    except ImageInfo.DoesNotExist:
        i = Image(image=filename)
        i.save()
        new = True
        colors = []
        try:
            colors = palette(path="%s%s" %
                             (settings.MEDIA_ROOT, i.image), mode="kmeans")
        except:
            try:
                colors = palette(path="%s%s" % (settings.MEDIA_ROOT, i.image))
            except:
                pass
        for c in colors:
            pal, cr = Pallete.objects.get_or_create(color=c)
            i.pallete.add(pal)

        if i.width < 600:
            print("TOO small, ", i.width)
            i.is_active = False
            i.save()
        ii = ImageInfo(image=i, external_url=ex_url, ahash=ahash, dhash=dhash,
                       phash=phash)
        ii.save()

        # rimages = ImageInfo.objects.filter(Q(phash=ii.phash)|Q(dhash=ii.dhash)).exclude(pk=ii.pk)
        # for ri in rimages:
        #    print "phash %s - %s, dhash %s - %s" % (ri.phash, ii.phash, ri.dhash, ii.dhash)
        #    si = SimiliarImage.objects.get_or_create(im1=ri.image, im2=ii.image)
            # ii.image.is_active = False
            # ii.image.save(update_fields=['is_active'])
            # if ii.image.width > ri.image.width:
            #    ri.image.image = ii.image.image
            #    ri.image.save()

        #    print "Duplicate: %s, but added" % ii.image

        # rimages = ImageInfo.objects.filter(phash=ii.phash).count()
        # if rimages > 1:
        #    ii.image.is_active = False
        #    ii.image.save(update_fields=['is_active'])
    else:
        print("Dhash, phash dublicate!")
        print(filepath)
        os.remove(filepath)

    for t in tags:
        if t != '':
            tag, c = Tag.objects.get_or_create(name=t)
            ii.tags.add(tag)
    
    if new:
        print(i.get_thumb_big())
        return i
    return False
