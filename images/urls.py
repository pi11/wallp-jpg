# -*- coding: utf-8 -*-
from django.conf.urls import url
from images.views import *
from images.feeds import LatestEntriesFeed

urlpatterns = [
    url(r'^listhash/(?P<hsh>\w+)/(?P<hsh_type>\w{1})/$',
        hash_images, name="hash_images"),

    url(r'^f/r/(?P<image_id>\d+)$', find_kind, name="find_kind"),

    url(r'^$', index, name="index"),
    url(r'^top/$', top, name="top"),
    url(r'^new/$', new, name="new"),
    url(r'^popular/$', popular, name="popular"),
    url(r'^s/(?P<width>\d+)x(?P<height>\d+)/$', by_size, name="by_size"),
    url(r'^t/(?P<tag_id>\d+)$', by_tag, name="by_tag"),
    url(r'^c/(?P<color_id>[\w\d]+)$', by_color, name="by_color"),

    url(r'^cr/(?P<image_id>\d+)$', color_related, name="color_related"),


    url(r'^sizes/$', sizes, name="sizes"),
    # url(r'^colors/$', colors, name="colors"),

    url(r'^random/$', random, name="random"),
    url(r'^top/(?P<interval>\w+)/$', top_interval, name="top_interval"),
    url(r'^newtitle/$', newtitle, name="newtitle"),
    url(r'^update/$', update, name="update"),
    url(r'^vote/(?P<image_id>\d+)$', vote, name="vote"),
    url(r'^(?P<image_id>\d+)$', view_image, name="view_image"),
    url(r'^news/$', LatestEntriesFeed(), name="main_feed"),

    url(r'^set-favorite/(?P<image_id>\d+)/$',
        favorite_image, name="favorite_image"),

    url(r'^favorite/$', favorite, name="favorite"),


    url(r'^login/$', login_view, name='login'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^auth/(?P<user_id>\d+)/(?P<token>\w+)/$', auth, name='auth'),


    url(r'^size/(?P<width>\d+)x(?P<height>\d+)/$',
        redirect_size, name="redirect_size"),
    url(r'^search/$', search_tag, name="search"),
    url(r'^live/$', live, name="live"),

]
