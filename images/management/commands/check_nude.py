# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import imagehash
import PIL
import os, time
from imagehash import hex_to_hash
import nude
from nude import Nude

from images.models import *

class Command(BaseCommand):
    args = ''
    help = 'Update images hashes'

    def handle(self, *args, **options):
        for im in Image.objects.filter(is_processed=False)[:100]:
            if os.path.exists(im.image.path):
                print im.image.path
                if nude.is_nude(str(im.image.path)):
                    print ("Nude! %s" % im.image.url)
                    im.nsfw = True
                else:
                    print ("Not Nude! %s" % im.image.url)
                    im.nsfw = False
                
                im.is_processed = True
                im.save(update_fields=['is_processed', 'nsfw', ])

            
