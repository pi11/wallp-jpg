# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import imagehash
import PIL
import os, time
from imagehash import hex_to_hash

from images.models import *

class Command(BaseCommand):
    args = ''
    help = 'Update images hashes'

    def handle(self, *args, **options):
        print "Script will delete images with dublicate hashes"
        print "======================================================"
        print
        
        for i in ImageInfo.objects.filter(image__is_active=True).order_by('-id'):
            for ri in ImageInfo.objects.filter(image__is_active=True).exclude(pk=i.pk).order_by('up_vote'):
                if hex_to_hash(i.dhash) - hex_to_hash(ri.dhash) < 4:
                    if ri.up_vote <= i.up_vote:
                        if ri.image.title is None or ri.image.title == '':
                            print "Deleting... %s " % ri
                            ri.image.delete()
                            ri.delete()
                    

        return
    
        for i in ImageInfo.objects.filter(image__is_active=True).order_by('-id'):
            for ri in ImageInfo.objects.filter(image__is_active=True, dhash=i.dhash).exclude(pk=i.pk).order_by('up_vote'):
                if ri.up_vote <= i.up_vote:
                    if ri.image.title is None or ri.image.title == '':
                        print "Deleting... %s " % ri
                        ri.image.delete()
                        ri.delete()


            for ri in ImageInfo.objects.filter(image__is_active=True, phash=i.phash).exclude(pk=i.pk).order_by('up_vote'):
                if ri.up_vote <= i.up_vote:
                    if ri.image.title is None or ri.image.title == '':

                        print "Deleting... %s " % ri
                        ri.image.delete()
                        ri.delete()
