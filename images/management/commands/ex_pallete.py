# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import imagehash
import PIL
import os, time
from imagehash import hex_to_hash
from colorweave import palette
from webcolors import hex_to_rgb
import numpy
from images.models import *

def ColorDistance(rgb1,rgb2):
    '''d = {} distance between two colors(3)'''
    rm = 0.5*(rgb1[0]+rgb2[0])
    d = sum((2+rm,4,3-rm)*(rgb1-rgb2)**2)**0.5
    return d

class Command(BaseCommand):
    args = ''
    help = 'Update images hashes'

    def handle(self, *args, **options):
        for p in Pallete.objects.all():
            for p2 in Pallete.objects.filter().exclude(pk=p.pk):
                d = ColorDistance(numpy.array(hex_to_rgb(p.color)), numpy.array(hex_to_rgb(p2.color)))
                try:
                    dis = float(d)
                except VallueError:
                    pass
                else:
                    if dis < 2.5:
                        print p.color, p2.color
                        p.near_colors.add(p2)
