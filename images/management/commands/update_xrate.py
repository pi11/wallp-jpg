# -*- coding: utf-8 -*-

import string, sys

from django.core.management.base import BaseCommand, CommandError
from django.utils.html import strip_tags

from images.models import *

from djangohelpers.utils import wilson_score

class Command(BaseCommand):
    args = ''
    help = 'Find duplicates'     



    def handle(self, *args, **options):
        for gr in Image.objects.all():
            try:
                gr.xrate = wilson_score(gr.imageinfo.up_vote, gr.imageinfo.up_vote + gr.imageinfo.down_vote)
            except:
                pass
            else:
                gr.save(update_fields=['xrate'])
