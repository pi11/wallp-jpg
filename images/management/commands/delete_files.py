# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import os, sys

from django.core.management.base import BaseCommand, CommandError

from images.models import *

class Command(BaseCommand):
    args = ''
    help = 'Delete old images'

    def handle(self, *args, **options):
        exist = 0
        nonexist = 0
        for d, dirs, files in os.walk(args[0]):
            for f in files:
                fp = os.path.join(d, f)
                fpc = fp.replace(args[0], '')
                if "scale_" in fp:
                    continue
                c = Image.objects.filter(image=fpc).count()
                if c > 0:
                    exist += 1
                else:
                    nonexist += 1
                    os.remove(fp)
                print "Test - %s - %s" % (fp, c)

        print "Total files exists: %s, not exists: %s" % (exist, nonexist)
