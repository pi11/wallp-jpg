# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import os

from django.core.management.base import BaseCommand, CommandError

from images.models import *

class Command(BaseCommand):
    args = ''
    help = 'Delete old images'

    def handle(self, *args, **options):
        for i in Image.objects.filter():
            if os.path.exists(i.image.path):
                pass
            else:
                i.delete()
