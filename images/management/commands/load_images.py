# -*- coding: utf-8 -*-
import re
import os
import hashlib
from datetime import date, timedelta, datetime
import pytz
import time
import random
import PIL, imagehash

from django.core.management.base import BaseCommand, CommandError
from django.utils.html import strip_tags
from django.contrib.auth.models import User
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.conf import settings

from images.models import *
from images.functions import process_image
from djangohelpers.url import load_url
from djangohelpers.utils import gen_url
from django.utils import timezone


def get_date_path():
    d = date.today()
    return "%s-%s-%s" % (d.year, d.month, d.day)

def save(data, filename):
    try:
        f = open(filename ,'w+')
        f.write(data)
        f.close()
    except IOError, e:
        print "Error saving file %s" % filename
        pass

def is_naive(dt):
    """Return :const:`True` if the datetime is naive
    (does not have timezone information)."""
    return dt.tzinfo is None or dt.tzinfo.utcoffset(dt) is None

class Command(BaseCommand):
    args = ''
    help = 'Download images'

    def handle(self, *args, **options):
        #tzi = pytz.utc #timezone('US/Eastern')
        tzi = pytz.timezone(settings.TIME_ZONE)
        for s in Source.objects.filter(is_active=True):
            delta = timedelta(minutes=s.interval)   
            print "Loading %s" % s
            print "Last checked: %s" % s.last_checked
            print "Now: %s " % datetime.now(tz=tzi)
            print "Delta: %s " % delta
            skip = False
            if is_naive(s.last_checked):
                if s.last_checked > datetime.now() - delta:
                    skip = True
            else:
                awt = timezone.make_aware(s.last_checked, timezone.get_default_timezone())
                if awt > datetime.now(tz=tzi) - delta:
                    skip = True
            if skip:
                print "skipp..."
                continue
            print "Go..."
            #time.sleep(1 + random.randint(1, 5))
            new = False
            s.last_checked = datetime.now(tz=tzi)
            s.save()                                   
            data = load_url(s.url)
            if s.dot_all:
                f_re = re.compile(r'%s' % s.regexp, re.DOTALL)
            else:
                f_re = re.compile(r'%s' % s.regexp)
            urls = f_re.findall(data)
            print urls
            if len(urls) == 0:
                print urls, s.regexp
                print data
                
                s.error = "Looks like regexp failed, no urls found"
                s.save()
            for url in urls:
                print url
                if url[s.item_index].strip() == '':
                    print "Skiping empty url"
                    print data
                    #sys.exit()
                    continue
                print url[s.item_index]
                new_images = 0
                #time.sleep(1)
                if s.is_add_url:
                    #print type(url[s.item_index])
                    durl = url[s.item_index].decode("utf-8")#.decode("utf-8")
                    #print type(durl)
                    
                    
                    ex_url = u"%s%s" % (s.url, durl)
                    ex_url = ex_url.encode('utf-8')
                    print ex_url
                else:
                    ex_url = url[s.item_index]
                if s.follow_links:
                    new_data  = load_url(ex_url)
                    fsub1_re = re.compile(r'%s' % s.sub_regexp, re.DOTALL)
                    #print s.sub_regexp, fsub1_re.findall(new_data)
                    tmp_url = fsub1_re.findall(new_data)[s.item_index2]
                    #print "tmpurl", tmp_url
                    if s.sub_regexp2 is not None and s.sub_regexp2.strip() != "":
                        new_data = load_url(tmp_url)
                        fsub2_re = re.compile(r'%s' % s.sub_regexp2, re.DOTALL)
                        tmp_url = fsub2_re.findall(new_data)[s.item_index3]

                    ex_url = tmp_url
                    #print "Go: [%s]" % ex_url
                try:
                    ii = ImageInfo.objects.get(external_url=ex_url)
                except ImageInfo.DoesNotExist:
                    new = process_image(ex_url)
                    if new:
                        new_images += 1
            if new == True:
                #j = random.randint(new_images, 7)
                s.interval = s.interval - new_images - 5
                if s.interval < 10:
                    s.interval = 10
            else:
                j = random.randint(2, 7)
                s.interval = s.interval + j

            s.save()             
