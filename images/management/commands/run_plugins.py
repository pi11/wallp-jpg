#-*- coding: utf-8 -*-
# This command used to run all plugins

from datetime import datetime, timedelta
import traceback
import sys

from django.core.management.base import BaseCommand, CommandError
#from django.core.mail import mail_admins
from django.conf import settings

from images.models import Plugin


class Command(BaseCommand):
    """
    This command used to run all plugins
    """
    help = """This command used to run all plugins"""

    def handle(self, *args, **options):

        plugins = Plugin.objects.filter(runing=1, status=1)
        for pl in plugins:
            if pl.last_start + timedelta(days=2) < datetime.now():
                print ("Plugin freezes, fixing")
                pl.runing = 0
                pl.save()
                

        filters = {}
        filters['status'] = 1
        filters['runing'] = 0 # Only not runing plugins
        only_top = False
        if len(args) > 0:
            filters['name'] = args[0]
            
            try:
                j = args[3]
                if j == "top":
                    only_top = True
            except IndexError:
                pass
            force_start = True
        else:
            force_start = False

        manual_pages = False
        if len(args) > 1:
            first_page = int(args[1])
            last_page = int(args[2])
            manual_pages = True

        print "Plugin manager started..."
        print "*" * 30

        # check for hanging plugins:
        n = datetime.now() - timedelta(minutes=20)
        for p in Plugin.objects.filter(runing=1):
            if p.last_start < n:
                p.runing = 0
                p.save()

        plugins = Plugin.objects.filter(**filters) #[:3].order_by("last_start")[:3]
        if len(plugins) == 0:
            print "No plugins found..."
        for plugin in plugins:
            n = datetime.now() - timedelta(minutes=plugin.frequency)
            if plugin.last_start > n and force_start == False:
                print ("Skiping plugin: %s, Freq: %s minutes,"
                       " Last start: %s") % (plugin, plugin.frequency,
                                             plugin.last_start)
                continue # skip plugins whose time not come
            
            plugin.runing = 1
            plugin.last_start = datetime.now()
            plugin.save()

            print "Runing: %s" % plugin.name
            imp = "plugins.%s" % plugin.name
            pl = __import__(imp, fromlist=["plugins",])

            new_content = False
            if manual_pages == False:
                first_page = plugin.first_page
                last_page = plugin.last_page

            for page in range(first_page, last_page, plugin.step):
                print "Page: %s" % page
                print "-=" * 30
                try:
                    
                    nc = pl.handle(page, only_top=only_top)
                    if nc:
                        new_content = True
                except TypeError:
                    nc = pl.handle(page)
                    if nc:
                        new_content = True
                    
                except:
                    error = traceback.format_exc()
                    print error
                    plugin.error = error
                    plugin.runing = 0
                    plugin.save()
                    raise

            if new_content:
                plugin.frequency = plugin.frequency - 5
                plugin.last_update = datetime.now()
            else:
                plugin.frequency = plugin.frequency + 5
            if plugin.frequency < 6:
                plugin.frequency = 5 # minimum frequency in minutes
            if plugin.frequency > 120: 
                plugin.frequency = 120 # max frequency in minutes
            plugin.runing = 0
            plugin.save()
