# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from images.models import *


class Command(BaseCommand):
    args = ''
    help = 'Count tags'

    def handle(self, *args, **options):
        for t in Tag.objects.all():
            t.count = ImageInfo.objects.filter(tags=t).count()
            t.save()
