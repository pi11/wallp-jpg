# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from images.models import *

class Command(BaseCommand):
    args = ''
    help = 'Update/save images'

    def handle(self, *args, **options):
        for ii in Image.objects.all():
            ii.save()
