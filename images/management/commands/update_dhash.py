# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import imagehash
import PIL
import os, time

from images.models import *

class Command(BaseCommand):
    args = ''
    help = 'Update images hashes'

    def handle(self, *args, **options):
        print "Script will update image hashes - dhash, phash, ahash."
        print "All images without files will be deleted."
        print "Waiting 5 seconds before start..."
        print "======================================================"
        print 
        time.sleep(5)
        for i in ImageInfo.objects.filter(dhash__isnull=True).order_by('id'):
            f = "%s%s" % (settings.MEDIA_ROOT, i.image.image)

            if os.path.exists(f):
                print "Processing image: %s " % f
                image = PIL.Image.open(f)
                i.dhash = imagehash.dhash(image)
                i.ahash = imagehash.average_hash(image)
                i.phash = imagehash.phash(image)
                i.save(update_fields=['dhash', 'ahash', 'phash'])
            else:
                print "File %s not found, deleting image object." % f
                im = i.image
                i.delete()
                im.delete()
