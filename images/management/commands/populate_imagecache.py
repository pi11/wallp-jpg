# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db import connection

class Command(BaseCommand):
    args = ''
    help = 'Update images cache'

    def handle(self, *args, **options):
        cursor = connection.cursor()
        cursor.execute("delete from images_imagecache")        
        cursor.execute("""
        insert into images_imagecache (h_id, i_id_id) 
        (select row_number() OVER (), id from 
        images_image order by id)""")
