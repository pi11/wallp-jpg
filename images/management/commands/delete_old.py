# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import os

from django.core.management.base import BaseCommand, CommandError

from images.models import *

class Command(BaseCommand):
    args = ''
    help = 'Delete old images'

    def handle(self, *args, **options):
        d = datetime.now() - timedelta(weeks=4)
        for i in Image.objects.filter(added__lte=d, pvote__lte=10):
            print "Deleting - %s" % i.image
            try:
                ii = i.imageinfo
                ii.delete()
            except ImageInfo.DoesNotExist:
                pass
            sc = "%s_scale_500x1200.jpg" % i.image.path[:-4]
            print "deleting - %s" % sc
            try:
                os.remove(sc)
                os.remove(i.image.path)
            except OSError:
                print "Scale not found"
            i.delete()
