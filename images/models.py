# -*- coding: utf-8 -*-

import pytz
from datetime import datetime
import traceback

from django import forms
from django.db import models
from django.conf import settings
from django.forms import ModelForm
from django.urls import reverse
from django.contrib.auth.models import User
from django.db import connection

from djangohelpers.utils import humanizeTimeDiff
from djangohelpers.images import scale

class BlockedTag(models.Model):
    name = models.CharField(max_length=100, unique=True)
    added = models.DateTimeField(auto_now_add=True, editable=False)
    

class Pallete(models.Model):
    color = models.CharField(max_length=7, unique=True)
    near_colors = models.ManyToManyField('self')

    def get_color(self):
        return self.color[1:]

    def __unicode__(self):
        return self.color

class ImageSize(models.Model):
    size = models.CharField(unique=True, max_length=15)
    count = models.IntegerField(default=0)
    
    
class ImageCache(models.Model):
    """Table for fast order by ? selection"""
    h_id = models.IntegerField(unique=True)
    i_id = models.ForeignKey('Image', on_delete=models.CASCADE)

class Image(models.Model):
    title = models.CharField(max_length=250, null=True, blank=True)
    added = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to="%Y-%m-%d/", height_field='height',
                              width_field='width')
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)
    pvote = models.IntegerField(default=0)
    xrate = models.FloatField(default=0)

    nsfw = models.BooleanField(default=False)
    gif = models.BooleanField(default=False)

    pallete = models.ManyToManyField(Pallete)

    thumb_created = models.BooleanField(default=False)
    big_thumb_created = models.BooleanField(default=False)

    is_processed = models.BooleanField(default=False)

    def is_gif(self):
        if self.image.url.lower()[-4:] == ".gif":
            return True
        else:
            return False

    def get_thumb(self):
        try:
            if self.thumb_created:
                check_path = False
            else:
                check_path = True
            thumb = scale(self.image, "%sx%s" % (settings.THUMB_WIDTH,
                                                 settings.THUMB_HEIGHT),
                          check_path=check_path)
            if thumb and self.thumb_created == False:
                self.thumb_created = True
                self.save(update_fields=['thumb_created', ])
        except:
            print(traceback.format_exc())
            thumb = False
        return thumb

    def get_admin_thumb(self):
        try:
            thumb = scale(self.image, "%sx%s" % (150, 150))
        except:
            print(traceback.format_exc())
            thumb = False
        return "<img src='%s' />" % thumb

    get_admin_thumb.allow_tags = True

    def get_thumb_big(self):
        try:
            if self.big_thumb_created:
                check_path = False
            else:
                check_path = True
            thumb = scale(self.image, "%sx%s" % (settings.BIG_THUMB_WIDTH,
                                                 settings.BIG_THUMB_HEIGHT),
                          check_path=check_path)
            if thumb and self.big_thumb_created == False:
                self.big_thumb_created = True
                self.save(update_fields=['big_thumb_created', ])

        except:
            print(traceback.format_exc())
            thumb = False
        return thumb

    def get_date(self):
        try:
            a = humanizeTimeDiff(self.added, pytz.timezone(settings.TIME_ZONE))
        except:
            a = humanizeTimeDiff(
                self.added)  # , pytz.timezone(settings.TIME_ZONE))
        return a

    def __unicode__(self):
        if self.title:
            return self.title
        else:
            return str(self.pk)

    def get_info(self):
        return ImageInfo.objects.get(image=self)

    def get_total_votes(self):
        ii = self.get_info()
        return ii.down_vote + ii.up_vote

    def get_vote(self):
        ii = ImageInfo.objects.get(image=self)
        total = ii.up_vote + ii.down_vote
        if total > 0:
            p = int((float(ii.up_vote) / float(total)) * 100)
        else:
            p = 0
        return p

    def get_absolute_url(self):
        url = reverse('view_image', kwargs={"image_id": self.pk})
        return url

    def get_pk(self):
        return self.pk

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def save(self, *args, **kwargs):
        if self.pk is None:
            if self.is_gif():
                self.gif = True


        if self.id is None:
            new = True
        else:
            new = False
        v = super(Image, self).save(*args, **kwargs)

        if new:
            cursor = connection.cursor()
            cursor.execute("SELECT max(h_id) FROM images_imagecache")
            row = cursor.fetchone()
            if row[0] != None:
                h_id = int(row[0]) + 1
            else:
                h_id = 0
            cursor.execute("INSERT into images_imagecache (h_id, "
                           "i_id_id) values (%s, %s)" % (h_id, self.id))

        return v

    def delete(self, *args, **kwargs):
        """Update images_imagecache while delete image"""
        #print "DELKET!!!"
        d_id = self.id
        cursor = connection.cursor()
        cursor.execute("SELECT  i_id_id, h_id FROM images_imagecache where "
                       "h_id = (select max(h_id) from images_imagecache)")
        row = cursor.fetchone()

        if row[0] != None:
            max_w_id = int(row[0])
            if max_w_id == self.id:
                cursor.execute(
                    "delete from images_imagecache where i_id_id = %s " % self.id)
                # pass
            else:
                cursor.execute(
                    "SELECT h_id FROM images_imagecache where i_id_id = %s" % self.id)
                row = cursor.fetchone()
                if row is not None:
                    old_h_id = row[0]
                    cursor.execute(
                        "delete from images_imagecache where i_id_id = %s " % self.id)
                    cursor.execute("UPDATE images_imagecache set h_id=%s"
                                   " where i_id_id=%s " % (old_h_id, max_w_id))
                else:
                    pass

        super(Image, self).delete(*args, **kwargs)


    class Meta:
        index_together = [
            ("width", "height", "is_active"),
        ]        

class SimiliarImage(models.Model):
    im1 = models.ForeignKey(Image, related_name='im1', on_delete=models.CASCADE)
    im2 = models.ForeignKey(Image, related_name='im2', on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s - %s" % (self.im1.pk, self.im2.pk)

    def thumb1(self):
        return self.im1.get_admin_thumb()

    def thumb2(self):
        return self.im2.get_admin_thumb()

    thumb1.allow_tags = True
    thumb2.allow_tags = True


class FavoriteImage(models.Model):
    im = models.ForeignKey(Image, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    added = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s - %s" % (self.user, self.im.image)

    class Meta:
        unique_together = ("im", "user")


class ImageTitle(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    vote = models.IntegerField(default=0)
    added = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        all_titles = ImageTitle.objects.filter(
            image=self.image).order_by("-vote", "-added")
        # print all_titles.query
        if len(all_titles) == 0:
            title = self.title
        else:
            if all_titles[0].vote > 0 or self.id is not None:
                title = all_titles[0].title
            else:
                title = self.title
                # print all_titles[0]
        im = self.image
        im.title = title
        im.save()

        v = super(ImageTitle, self).save(*args, **kwargs)
        return v


class ImageTitleForm(ModelForm):

    class Meta:
        model = ImageTitle
        exclude = ("vote", "added")


class Tag(models.Model):
    name = models.CharField(max_length=100, unique=True)
    ru_name = models.CharField(max_length=50, null=True, blank=True)
    count = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name


class ImageInfo(models.Model):
    # datahash = models.CharField(max_length=512, unique=True)
    image = models.OneToOneField(Image, on_delete=models.CASCADE)
    external_url = models.CharField(max_length=600, null=True, blank=True)
    views = models.IntegerField(default=0)
    today_views = models.IntegerField(default=0)
    up_vote = models.IntegerField(default=0)
    down_vote = models.IntegerField(default=0)

    vote = models.IntegerField(default=0)

    dhash = models.CharField(max_length=20)
    phash = models.CharField(max_length=20)
    ahash = models.CharField(max_length=20)

    tags = models.ManyToManyField(Tag, editable=False)

    class Meta:
        unique_together = ("dhash", "phash")  # 1 vote per day for ip

    def save(self, *args, **kwargs):
        total = self.up_vote + self.down_vote
        if total > 0:
            p = int((float(self.up_vote) / float(total)) * 100)
        else:
            p = 0
        self.vote = p
        im = self.image
        im.pvote = p
        if p < 40 and self.up_vote + self.down_vote > 4:
            im.is_active = False
        im.save()
        v = super(ImageInfo, self).save(*args, **kwargs)
        return v

    def __unicode__(self):
        return unicode(self.image)


class Vote(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    vote = models.PositiveSmallIntegerField(
        default=0)  # 0-down vote, 2-up vote
    ip = models.GenericIPAddressField()
    added = models.DateTimeField(auto_now=True)
    day_added = models.DateField(auto_now=True)

    class Meta:
        unique_together = ("image", "ip", "day_added")  # 1 vote per day for ip

    def __unicode__(self):
        return unicode(self.image)


class Source(models.Model):
    url = models.CharField(max_length=300, unique=True)
    regexp = models.TextField()
    sub_regexp = models.TextField(null=True, blank=True)
    sub_regexp2 = models.TextField(null=True, blank=True)
    item_index = models.PositiveSmallIntegerField(default=0)
    item_index2 = models.PositiveSmallIntegerField(default=0)
    item_index3 = models.PositiveSmallIntegerField(default=0)
    last_checked = models.DateTimeField()
    is_active = models.BooleanField()
    follow_links = models.BooleanField(default=False)
    error = models.TextField(null=True, blank=True)
    interval = models.IntegerField(default=10)
    dot_all = models.BooleanField(default=True)
    is_add_url = models.BooleanField(default=False)

    def __unicode__(self):
        return self.url


class Plugin(models.Model):

    """
    Plugin model

    Administrator can add/edit/delete crawl plugins here
    site_name can be any name. It is used to easy find plugin.
    """
    STATUSES = ((0, 'Not active'),
                (1, 'Active'),)

    RUN_STATUSES = ((0, 'Not runing'),
                   (1, 'Runing'),)

    name = models.CharField(max_length=150, verbose_name="Name")
    last_start = models.DateTimeField(default=datetime.now)
    last_update = models.DateTimeField(default=datetime.now)
    date_added = models.DateTimeField(auto_now_add=True)

    status = models.IntegerField(choices=STATUSES,
                                 verbose_name="Status of plugin", default=1)
    runing = models.IntegerField(choices=RUN_STATUSES,
                                 verbose_name="Run status of plugin", default=0)
    error = models.TextField(blank=True, null=True)
    frequency = models.IntegerField(default=30)

    first_page = models.IntegerField(default=1)
    last_page = models.IntegerField(default=2)
    step = models.IntegerField(default=1)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name", ]
        verbose_name_plural = "Plugins"
