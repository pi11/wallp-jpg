# -*- coding: utf-8 -*-

from django.urls import reverse
from django.contrib.syndication.views import Feed
from django.conf import settings
from images.models import *

class LatestEntriesFeed(Feed):
    title = settings.RSS_TITLE #u"Приколы на JPG-1.com"
    link = "/"
    description = settings.RSS_DESCRIPTION #"Новые приколы на jpg-1.com"

    def items(self):
        return Image.objects.filter(pvote__gt=60,
                                    is_active=True,
                                    imageinfo__up_vote__gt=2).order_by('-id')[:15]

    def item_title(self, item):
        if item.title and item.title != '':
            return item.title
        else:
            return ""

    def item_enclosure_url(self, item):
        """
        Takes an item, as returned by items(), and returns the item's
        enclosure URL.
        """
        return item.image.url


    def item_pubdate(self, item):
        """
        Takes an item, as returned by items(), and returns the item's
        pubdate.
        """
        return item.added

    def item_description(self, item):
        return ""
    
    def item_link(self, item):
        return reverse('view_image', kwargs={"image_id":item.pk})
    
    def item_enclosure_length(self, item):
        """
        Takes an item, as returned by items(), and returns the item's
        enclosure length.
        """
        try:
            s = item.image.size
        except OSError:
            s = 0
        return 
        
    item_enclosure_mime_type = "image/jpg"
